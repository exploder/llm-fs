import re

from transformers import AutoModelForCausalLM, AutoTokenizer


def main():
    model_path = 'models/old-btwos'
    model = AutoModelForCausalLM.from_pretrained(model_path).cuda()
    tokenizer = AutoTokenizer.from_pretrained(model_path, max_length=1024, truncation=True)

    prompt = '<path>/usr/lib</path>'
    inputs = tokenizer(prompt, return_tensors='pt')
    input_ids = inputs['input_ids'].cuda()
    attention_mask = inputs.get('attention_mask', None)
    if attention_mask is not None:
        attention_mask = attention_mask.cuda()

    prompt_len = len(input_ids)

    outputs = model.generate(
        input_ids,
        attention_mask=attention_mask,
        max_new_tokens=1000,
        temperature=0.2,
        do_sample=True,
        # num_beams=4,
        # penalty_alpha=0.6,
        # top_k=4,
        top_p=0.9,
        repetition_penalty=1.2,
    )

    output = tokenizer.decode(outputs[0], skip_special_tokens=False)
    print(prompt)
    print('\n'.join(re.split(r'<file>|<dir>', output[len(prompt):].replace('<|endoftext|>', ''))))


if __name__ == '__main__':
    main()
