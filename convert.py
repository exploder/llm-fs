import argparse
import json
import pathlib
import random


def process_dir(gdu_data: list, base='') -> list:
    if base.endswith('/') or base == '':
        base_path = f'{base}{gdu_data[0]["name"]}'
    else:
        base_path = f'{base}/{gdu_data[0]["name"]}'

    data = []
    files = []

    for file in gdu_data[1:]:
        if isinstance(file, list):
            data += process_dir(file, base_path)
            files.append({
                'name': file[0]['name'],
                'is_dir': True,
            })
        elif isinstance(file, dict):
            files.append({
                'name': file["name"],
                'is_dir': False,
                'size': file.get('asize', 0)
            })

    data.append({
        'path': base_path,
        'files': files,
    })

    return data


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', type=pathlib.Path, help='Path to the JSON file produced by gdu')
    parser.add_argument('output', type=pathlib.Path, help='Path to the output JSON file')
    parser.add_argument('--no-shuffle', action='store_true', help='Do not shuffle the output')

    args = parser.parse_args()

    print(f'Reading GDU data from {args.input}')
    with open(args.input, 'r') as f:
        gdu_data = json.load(f)

    print('GDU data loaded, converting')
    data = process_dir(gdu_data[3])
    print(f'Conversion complete, found {len(data)} directories - writing output to {args.output}')

    if not args.no_shuffle:
        random.shuffle(data)

    with open(args.output, 'w') as f:
        json.dump(data, f)


if __name__ == '__main__':
    main()
